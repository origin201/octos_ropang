using System;
using System.Collections;
using System.Net;
using System.Net.Sockets;
using UnityEngine;

namespace Jois
{
    public class JTcpSocket : MonoBehaviour
    {
        private TcpClient _tcpClient;
        private NetworkStream _networkStream;
        private IPHostEntry _ipHostEntry;

        private string _hostname;
        private int _port;

        byte[] _readBuffer = new byte[MaxReadSize];

        private int _sizeHeaderSize; // 0, 2, 4

        private int _readOffset;
        private int _leftSize;

        private Action<bool> _onConnectionFinish;
        public Action<byte[], int> _onRead;

        private const int MaxReadSize = 1024 * 8;

        void Update()
        {
            if (_networkStream.CanRead && _networkStream.DataAvailable)
            {
                ReadMessage();
            }
        }

        public void Connect(string hostname, int port, Action<bool> onFinish, int sizeHeaderSize = 0,
            float timeout = 5F)
        {
            _hostname = hostname;
            _port = port;
            _onConnectionFinish = onFinish;
            _sizeHeaderSize = sizeHeaderSize;

            try
            {
                #if false
                _ipHostEntry = Dns.GetHostEntry(hostname);
                //_tcpClient = new TcpClient(AddressFamily.InterNetworkV6);
                _tcpClient = new TcpClient(AddressFamily.InterNetwork);
                _tcpClient.Connect(_ipHostEntry.AddressList, port);
#else
                _tcpClient = new TcpClient();
                _tcpClient.Connect(hostname, port);
                #endif
                StartCoroutine(CheckConnected(0.2F, timeout));
            }
            catch (ArgumentNullException e)
            {
                Debug.LogFormat("ArgumentNullException: {0} ", e);
                _onConnectionFinish(false);
                Disconnect();
            }
            catch (SocketException e)
            {
                Debug.LogFormat("SocketException-> {0} ", e);
                _onConnectionFinish(false);
                Disconnect();
            }
        }

        public void Disconnect()
        {
        }

        public void WriteMessage(byte[] buf, int size)
        {
            try
            {
                NetworkStream stream = _tcpClient.GetStream();
                if (stream != null && stream.CanWrite)
                {
                    // Write byte array to socketConnection stream.
                    stream.Write(buf, 0, size);
                }
                else
                {
                    Debug.LogError("Could not TcpClientBase.WriteMessage \n");
                }
            }
            catch (SocketException socketException)
            {
                Debug.Log("Socket exception-> " + socketException);
            }
        }

        void ReadMessage()
        {
            Debug.AssertFormat(_leftSize > 0, "should leftSize > 0, but {0}", _leftSize);

            int readSize = _networkStream.Read(_readBuffer, _readOffset, _leftSize);
            _readOffset += readSize;

            if (_sizeHeaderSize == 0)
            {
                _onRead(_readBuffer, _readOffset);
            }
            else
            {
                int packetLength = (int) (_sizeHeaderSize == 2
                    ? JLittleEndian.Read_u16(_readBuffer)
                    : JLittleEndian.Read_u32(_readBuffer));

                _leftSize = packetLength - _sizeHeaderSize;

                while (_leftSize > 0)
                {
                    readSize = _networkStream.Read(_readBuffer, _readOffset, _leftSize);
                    _readOffset += readSize;
                    _leftSize -= readSize;
                }

                // Complete
                if (_leftSize == 0)
                {
                    _onRead(_readBuffer, _readOffset);

                    _readOffset = 0;
                    _leftSize = _sizeHeaderSize;
                }
            }
        }

        IEnumerator CheckConnected(float checkInterval, float timeout)
        {
            int count = (int) ((timeout + 0.001F) / checkInterval);
            float elampsedTime = 0f;

            for (int i = 0; i < count; ++i)
            {
                yield return new WaitForSeconds(checkInterval);

                elampsedTime += checkInterval;

                if (_tcpClient.Connected)
                {
                    Debug.LogFormat("u(-1) Successfully Connected to {0} {1} \n", _hostname, _port);

                    _networkStream = _tcpClient.GetStream();

                    this.enabled = true;

                    if (_onConnectionFinish != null)
                    {
                        _onConnectionFinish(true);
                        _onConnectionFinish = null;
                    }

                    yield break;
                }
                else
                {
                    if (elampsedTime >= timeout)
                    {
                        if (_onConnectionFinish != null)
                        {
                            _onConnectionFinish(false);
                            _onConnectionFinish = null;
                        }

                        Disconnect();
                        yield break;
                    }
                }
            }
        }
    }
}