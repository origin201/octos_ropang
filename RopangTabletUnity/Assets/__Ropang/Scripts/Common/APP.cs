﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;


namespace Ropang
{
    public class APP : MonoBehaviour
    {
        [SerializeField] private MessageCenter _messageCenter;

        [SerializeField] UdpAgent _tempUdpAgentRef;
        [SerializeField] private UdpServer _udpServer;

        public static MessageCenter MsgCenter { get; private set; }

        public static UdpServer Server { get; private set; }
        public static UdpAgent TempUdpAgentRef { get; private set; }

        #region Mono Singleton

        Guid _guid;

        private static APP _instance;
        static Guid _instanceGuid;

        void Awake()
        {
            Debug.Log("APP.Awake ");
            if (_instance == null)
            {
                _instance = this;
                InitStaticObjects();
                DontDestroyOnLoad(gameObject);

                _instanceGuid = Guid.NewGuid();
                _guid = _instanceGuid;
                Debug.LogFormat("APP Singleton has been created. guid({0}) ", _guid.ToString());
            }
            else
            {
                Destroy(gameObject);
            }
        }

        private void OnDestroy()
        {
            Debug.Log("APP.OnDestroy \n");
            if (_guid == _instanceGuid)
            {
                _instance = null;
                Debug.LogFormat("APP Singleton has been destroyed. guid({0}) ", _guid.ToString());
            }
        }

        #endregion


        private void OnApplicationQuit()
        {
        }

        void InitStaticObjects()
        {
            // Config = _config;
            // Data = _runtimeData;
            // ArAssetMgr = _arAssetManager;
            MsgCenter = _messageCenter;
            // // Audio = _audioMgr;
            // Logic = _logic;
            // AudioMgr = _audioMgr;

            Server = _udpServer;
            TempUdpAgentRef = _tempUdpAgentRef;
        }
    }
}