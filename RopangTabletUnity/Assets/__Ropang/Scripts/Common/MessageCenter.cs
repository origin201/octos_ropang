using System;
using UnityEngine;

namespace Ropang
{
    public class MessageCenter : MonoBehaviour
    {
        public Action<string> OnServerGetMessage;
        public Action<string> OnServerAddressGot;

        public Action<MsgStartOrderProcess> OnOrderMessage;
    }
}