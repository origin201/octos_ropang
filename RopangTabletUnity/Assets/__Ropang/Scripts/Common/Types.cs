using System;

namespace Ropang
{
    public static class Consts
    {
        public const int UDP_PORT = 9000;
        public const int TCP_PORT = 9001;
    }


    [Serializable]
    public class MsgTcpServerInfo
    {
        public string ipAddress;
        public int port;
    }
    
    //[Serializable]
    
}