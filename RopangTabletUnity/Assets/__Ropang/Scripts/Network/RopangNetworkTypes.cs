using System;
using UnityEngine;

namespace Ropang
{
    [Serializable]
    public class MsgBase
    {
        public string msgName;
    }

    [Serializable]
    public class MsgTest : MsgBase
    {
        public int code;

        public MsgTest()
        {
            msgName = "test";
        }
    }

    [Serializable]
    public class MsgTableServerAddress : MsgBase
    {
        public string ipAddress;
        public int port;

        public MsgTableServerAddress()
        {
            msgName = "TableServerAddress";
        }
    }

    [Serializable]
    public class MsgStartOrderProcess : MsgBase
    {
        public MsgStartOrderProcess()
        {
            msgName = "order";
        }
    }

    [Serializable]
    public class MsgOrderProcessEnded : MsgBase
    {
        public MsgOrderProcessEnded()
        {
            msgName = "OrderProcessEnded";
        }
    }
}