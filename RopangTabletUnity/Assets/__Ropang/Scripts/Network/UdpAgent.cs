using System;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Cysharp.Threading.Tasks;
using UnityEngine;

namespace Ropang
{
    public class UdpAgent : MonoBehaviour
    {
        private UdpClient socket_;
        private IPEndPoint _endPoint;

        //private const int PORT = 9000;

        void Start()
        {
            GetConnectionInfoAsync();
        }

        void Update()
        {
            //socket_.Available && socket_.Client.Available && socket_.Client.re
        }

        public void ConnectForBroadcast()
        {
            //IPAddress ipAddress = IPAddress.Parse("127.0.0.1"); //.Broadcast.MapToIPv4();
            IPAddress ipAddress = IPAddress.Broadcast.MapToIPv4();
            _endPoint = new IPEndPoint(ipAddress, Consts.UDP_PORT);
            socket_ = new UdpClient
            {
                // Client =
                // {
                //     SendTimeout = 900, 
                //     ReceiveTimeout = 900
                // }, 
                // // DontFragment = ,
                // EnableBroadcast = true,
                // // ExclusiveAddressUse = ,
                // // MulticastLoopback = ,
                // // Ttl = 
            };
        }

//
        async void GetConnectionInfoAsync()
        {
            await UniTask.Delay(TimeSpan.FromSeconds(1F));

            ConnectForBroadcast();

            await UniTask.Delay(TimeSpan.FromSeconds(1F));

            Debug.LogFormat("Try to send ");

            var msgSend = new MsgTest
            {
                code = 777
            };
            //Send(JsonUtility.ToJson(msgSend));
            SendMsg(msgSend);

            await UniTask.Delay(TimeSpan.FromSeconds(1F));

            var res = await ReceiveAsync();

            Debug.LogFormat("Udp Client Received ({0}) ", res);

            APP.MsgCenter.OnServerAddressGot(res);

            for (int i = 0; i < 10; ++i)
            {
                var res2 = await ReceiveAsync();

                Debug.LogFormat("Udp Client Received ({0}) ", res2);
            }
        }

        // public void Send(string msg)
        // {
        //     if (socket_ == null)
        //         return;
        //
        //     Debug.LogFormat("Sending ({0}) ", msg);
        //     var data = Encoding.UTF8.GetBytes(msg);
        //     socket_.Send(data, msg.Length, _endPoint);
        // }

        public void SendMsg(MsgBase msg)
        {
            var msgSend = JsonUtility.ToJson(msg);
            var data = Encoding.UTF8.GetBytes(msgSend);
            socket_.Send(data, data.Length, _endPoint);
        }

        public async UniTask<string> ReceiveAsync()
        {
            //var data = socket_.Receive(ref _endPoint);
            var data = await socket_.ReceiveAsync();
            var msg = Encoding.UTF8.GetString(data.Buffer);
            return await UniTask.FromResult(msg);
        }
    }
}