﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;
using Ropang;
using UnityEngine;

public class UdpServer : MonoBehaviour
{
    private Socket _socket = new Socket(AddressFamily.InterNetwork, SocketType.Dgram, ProtocolType.Udp);
    private const int bufSize = 8 * 1024;
    private State state = new State();
    private EndPoint epFrom = new IPEndPoint(IPAddress.Any, 0);
    private AsyncCallback recv = null;

    private Action<byte[]> mMsgCallBack = null;

    private byte[] _readToHandle;
    
    public class State
    {
        public byte[] buffer = new byte[bufSize];
    }

    private void Start()
    {
        string localIP = "Not available, please check your network settings!";

        IPHostEntry host = Dns.GetHostEntry(Dns.GetHostName());


        foreach (IPAddress ip in host.AddressList)
        {
            if (ip.AddressFamily == AddressFamily.InterNetwork)
            {
                localIP = ip.ToString();

                Debug.LogFormat("Local IP ({0}) ", localIP);
            }
        }


        Server("127.0.0.1", Consts.UDP_PORT,
            bytes =>
            {
                var msg = Encoding.Default.GetString(bytes);

                Debug.LogFormat("Udp Server Received ({0}) ", msg);

                APP.MsgCenter.OnServerGetMessage(msg);

                var msgSend = new MsgTableServerAddress
                {
                    ipAddress = localIP,
                    port = Consts.TCP_PORT
                };

                Send(JsonUtility.ToJson(msgSend));

                Debug.LogFormat("Udp Server Sent ({0}) ", JsonUtility.ToJson(msgSend));
            });
    }

    void Update()
    {
        if (_readToHandle != null)
        {
            mMsgCallBack(_readToHandle);
            _readToHandle = null;
        }
    }

    public void Close()
    {
        if (_socket != null && _socket.Connected)
        {
            _socket.Disconnect(true);
            _socket.Shutdown(SocketShutdown.Both);
        }
    }

    public void Server(string address, int port, Action<byte[]> msgCallBack)
    {
        _socket.SetSocketOption(SocketOptionLevel.IP, SocketOptionName.ReuseAddress, true);
        //_socket.Bind(new IPEndPoint(IPAddress.Parse(address), port));
        _socket.Bind(new IPEndPoint(IPAddress.Any, port));
        mMsgCallBack = msgCallBack;
        Receive();
    }

    public void Client(string address, int port)
    {
        _socket.Connect(IPAddress.Parse(address), port);
        Receive();
    }

    public void SendMsg(MsgBase msg)
    {
        var msgSend = JsonUtility.ToJson(msg);
        var data = Encoding.UTF8.GetBytes(msgSend);
        _socket.BeginSendTo(data, 0, data.Length, SocketFlags.None, epFrom, (ar) =>
        {
            State so = (State) ar.AsyncState;
            int bytes = _socket.EndSend(ar);
            Debug.LogFormat("SEND: {0}, {1}", bytes, msg.msgName);
        }, state);
    }
    
    public void Send(string text)
    {
        byte[] data = UTF8Encoding.UTF8.GetBytes(text);

        // _socket.BeginSend(data, 0, data.Length, SocketFlags.None, (ar) =>
        // {
        //     State so = (State) ar.AsyncState;
        //     int bytes = _socket.EndSend(ar);
        //     Console.WriteLine("SEND: {0}, {1}", bytes, text);
        // }, state);

        _socket.BeginSendTo(data, 0, data.Length, SocketFlags.None, epFrom, (ar) =>
        {
            State so = (State) ar.AsyncState;
            int bytes = _socket.EndSend(ar);
            Console.WriteLine("SEND: {0}, {1}", bytes, text);
        }, state);
    }

    private void Receive()
    {
        _socket.BeginReceiveFrom(state.buffer, 0, bufSize, SocketFlags.None, ref epFrom, recv = (ar) =>
        {
            State so = (State) ar.AsyncState;
            int bytes = _socket.EndReceiveFrom(ar, ref epFrom);
            //state.
            _socket.BeginReceiveFrom(so.buffer, 0, bufSize, SocketFlags.None, ref epFrom, recv, so);

            _readToHandle = new byte[bytes];
            Array.Copy(so.buffer, _readToHandle, bytes);

            // if (mMsgCallBack != null)
            // {
            //     mMsgCallBack(_readToHandle);
            // }
        }, state);
    }
}